import cv2 as cv
import numpy as np
import os

out_dir = "./dataset"

num_tiles_x = 14
num_tiles_y = 12
labels = np.array([l for l in "ABCDEF"])
data_file_name = "alpha_64_14.png"

num_labels = len(labels)
rows_per_label = num_tiles_y // num_labels
samples_per_label = num_tiles_x * rows_per_label

img = cv.imread(data_file_name)

cells = [np.hsplit(row, num_tiles_x) for row in np.vsplit(img, num_tiles_y)]
data = np.array(cells)
data = data.reshape(num_labels, samples_per_label,
                    data.shape[2], data.shape[3], data.shape[4])

for n, l in enumerate(labels):
    section = data[n]
    for i, image in enumerate(section):
        cv.imwrite(
            f"dataset/{l}/{str(i).rjust(len(str(samples_per_label-1)), '0')}.png", np.array(image))
