# Alphabet recognition using CNN
This notebook is an implementation of using Convolutional Neural Networks to recognize handwritten letters. The letters used is capital A to F.

Several different combinations of hidden layers are tested to make a comparison. It should be noted that the results vary a lot between each run of the program. 

# Results
A general trend is that the accuracy increases with a higher number of epochs. Though, especially the validation accuracy, does sometimes decrease from one epoch to the next which may be a symptom of overfitting.

## 3 hidden layers
With 3 hidden layers it looks like (16, 16, 16) has the lowest validation performance. (64, 32, 16) is the most accurate and has the lowest loss.
![screenshot](results/output_3.png)

## 2 hidden layers
For 2 hidden layers, the different compositions perform more similarly. 
![screenshot](results/output_2.png)

## 1-3 hidden layers
For a differing number of layers, both versions with a single hidden layer performed the worst.  It also looks like (64, 64) is more accurate than (8, 8, 8)
![screenshot](results/output_1-3.png)

## Contributors
Eivind Vold Aunebakk

